package com.fuzzywave.slippery.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.fuzzywave.desktop.DesktopAnalytics;
import com.fuzzywave.slippery.SlipperyGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new SlipperyGame(new DesktopAnalytics()), config);
	}
}
