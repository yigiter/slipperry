package com.fuzzywave.slippery.stage;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.fuzzywave.slippery.actor.EarthActor;
import com.fuzzywave.slippery.actor.GroundActor;
import com.fuzzywave.slippery.actor.RunnerActor;
import com.fuzzywave.slippery.util.Constants;
import com.fuzzywave.slippery.util.WorldUtils;


public class GameStage extends Stage {

    private World world;
    private EarthActor earthActor;
    private RunnerActor runnerActor;

    private final float TIME_STEP = 1 / 60f;
    private float accumulator = 0f;

    private Box2DDebugRenderer renderer;

    private Vector3 touchPoint;

    public GameStage() {
        super(new FitViewport(Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT,
                              new OrthographicCamera()));
        setUpWorld();
        setupCamera();
        setupTouchControlAreas();

        earthActor.setCameraPositionX(getViewport().getCamera().position.x);
        renderer = new Box2DDebugRenderer();
    }

    private void setUpWorld() {
        world = WorldUtils.createWorld();
        setUpGround();
        setUpRunner();
    }

    private void setUpGround() {
        earthActor = new EarthActor(world);
        addActor(earthActor);
        for (GroundActor groundActor : earthActor.getGroundArray()) {
            addActor(groundActor);
        }
    }

    private void setUpRunner() {
        runnerActor = new RunnerActor(WorldUtils.createRunner(world));
        addActor(runnerActor);
    }

    private void setupCamera() {
        getViewport().getCamera().position.set(getViewport().getWorldWidth() / 2,
                                               getViewport().getWorldHeight() / 2, 0f);
        getViewport().getCamera().update();
    }

    private void setupTouchControlAreas() {
        touchPoint = new Vector3();
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        // Fixed timestep
        accumulator += delta;

        while (accumulator >= delta) {
            world.step(TIME_STEP, 6, 2);
            accumulator -= TIME_STEP;
        }

        getViewport().getCamera().position.x = Math.max(getViewport().getCamera().position.x,
                                                        runnerActor.getWorldPosition().x);
        earthActor.setCameraPositionX(getViewport().getCamera().position.x);

        //TODO: Implement interpolation
    }

    @Override
    public void draw() {
        super.draw();
        renderer.render(world, getViewport().getCamera().combined);
    }

    @Override
    public boolean touchDown(int x, int y, int pointer, int button) {

        // Need to get the actual coordinates
        translateScreenToWorldCoordinates(x, y);

        runnerActor.applyForce();

        return super.touchDown(x, y, pointer, button);
    }

    private void translateScreenToWorldCoordinates(int x, int y) {
        getCamera().unproject(touchPoint.set(x, y, 0));
    }
}
