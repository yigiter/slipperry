package com.fuzzywave.slippery.screen;


import com.fuzzywave.slippery.SlipperyGame;
import com.fuzzywave.slippery.stage.GameStage;

public class GameScreen extends AbstractScreen{

    private GameStage stage;

    public GameScreen(SlipperyGame game) {
        super(game);
        stage = new GameStage();
    }

    @Override
    public void render(float delta) {

        stage.draw();
        stage.act(delta);
    }
}
