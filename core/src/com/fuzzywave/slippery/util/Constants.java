package com.fuzzywave.slippery.util;


import com.badlogic.gdx.math.Vector2;

public class Constants {

    public static final int SCREEN_WIDTH = 480;
    public static final int SCREEN_HEIGHT = 270;

    public static final int VIEWPORT_WIDTH = 20; // box2d meters.
    public static final int VIEWPORT_HEIGHT = 13;


    public static final Vector2 WORLD_GRAVITY = new Vector2(0, -10);

    public static final int GROUND_CHUNK_COUNT = 6;

    public static final float GROUND_X = 0;
    public static final float GROUND_Y = 0;
    public static final float GROUND_WIDTH = ((float) (VIEWPORT_WIDTH)) / (GROUND_CHUNK_COUNT / 2);
    public static final float GROUND_HEIGHT = 2f;
    public static final float GROUND_DENSITY = 1f;

    public static final float GROUND_CHUNK_DELETION_DISTANCE = VIEWPORT_WIDTH + GROUND_WIDTH;

    public static final float RUNNER_X = 2;
    public static final float RUNNER_Y = GROUND_Y + GROUND_HEIGHT;
    public static final float RUNNER_WIDTH = 2f;
    public static final float RUNNER_POLYGON_SCALE = RUNNER_WIDTH / 2;
    public static final float[] RUNNER_POLYGON =
            new float[]{-0.75f * RUNNER_POLYGON_SCALE, 1.0f * RUNNER_POLYGON_SCALE,
                    -1.0f * RUNNER_POLYGON_SCALE, 0.75f * RUNNER_POLYGON_SCALE,
                    -1.0f * RUNNER_POLYGON_SCALE, -0.75f * RUNNER_POLYGON_SCALE,
                    -0.75f * RUNNER_POLYGON_SCALE, -1.0f * RUNNER_POLYGON_SCALE,
                    0.75f * RUNNER_POLYGON_SCALE, -1.0f * RUNNER_POLYGON_SCALE,
                    1.0f * RUNNER_POLYGON_SCALE, -0.75f * RUNNER_POLYGON_SCALE,
                    1.0f * RUNNER_POLYGON_SCALE, 0.75f * RUNNER_POLYGON_SCALE,
                    0.75f * RUNNER_POLYGON_SCALE, 1.0f * RUNNER_POLYGON_SCALE};
    public static float RUNNER_DENSITY = 0.5f;
    public static final Vector2 RUNNER_LINEAR_IMPULSE = new Vector2(13f, 0);

}
