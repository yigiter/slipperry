package com.fuzzywave.slippery.util;


import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.fuzzywave.slippery.box2d.GroundUserData;
import com.fuzzywave.slippery.box2d.RunnerUserData;

public class WorldUtils {

    public static World createWorld() {
        return new World(Constants.WORLD_GRAVITY, true);
    }

    public static Body createGround(World world, float positionX) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(new Vector2(positionX, Constants.GROUND_Y));
        Body body = world.createBody(bodyDef);
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(Constants.GROUND_WIDTH / 2, Constants.GROUND_HEIGHT / 2);

        FixtureDef fdef = new FixtureDef();
        fdef.density = Constants.GROUND_DENSITY;
        fdef.shape = shape;
        fdef.restitution = 0f;
        fdef.friction = 1f;
        body.createFixture(fdef);

        body.setUserData(new GroundUserData());
        shape.dispose();
        return body;
    }

    public static Body createRunner(World world) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(new Vector2(Constants.RUNNER_X, Constants.RUNNER_Y));
        PolygonShape shape = new PolygonShape();
        shape.set(Constants.RUNNER_POLYGON);
        //shape.setAsBox(Constants.RUNNER_WIDTH / 2, Constants.RUNNER_HEIGHT / 2);
        Body body = world.createBody(bodyDef);

        FixtureDef fdef = new FixtureDef();
        fdef.density = Constants.RUNNER_DENSITY;
        fdef.shape = shape;
        fdef.restitution = 0f;
        fdef.friction = 0.2f;
        body.createFixture(fdef);

        body.resetMassData();
        body.setUserData(new RunnerUserData());
        shape.dispose();
        return body;
    }
}
