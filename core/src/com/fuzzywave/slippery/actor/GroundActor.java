package com.fuzzywave.slippery.actor;


import com.badlogic.gdx.physics.box2d.Body;
import com.fuzzywave.slippery.box2d.GroundUserData;

public class GroundActor extends GameActor{

    public GroundActor(Body body) {
        super(body);
    }

    @Override
    public GroundUserData getUserData() {
        return (GroundUserData) userData;
    }
}
