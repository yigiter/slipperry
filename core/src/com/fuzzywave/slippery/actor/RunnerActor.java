package com.fuzzywave.slippery.actor;


import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.fuzzywave.slippery.box2d.RunnerUserData;

public class RunnerActor extends GameActor {

    public RunnerActor(Body body) {
        super(body);
    }

    @Override
    public RunnerUserData getUserData() {
        return (RunnerUserData) userData;
    }

    public void applyForce() {
        Vector2 jumpingLinearImpulse = getUserData().getJumpingLinearImpulse();
        Vector2 center = body.getWorldCenter();
        body.applyLinearImpulse(jumpingLinearImpulse.x, jumpingLinearImpulse.y, center.x,
                                center.y + 0.01f, true);
    }
}
