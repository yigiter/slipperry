package com.fuzzywave.slippery.actor;

import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.fuzzywave.core.CoreLogger;
import com.fuzzywave.slippery.util.Constants;
import com.fuzzywave.slippery.util.WorldUtils;

public class EarthActor extends Actor {

    private World world;
    private Array<GroundActor> groundArray;
    private float lastGroundChunkPositionX = 0;
    private float cameraPositionX;


    public EarthActor(World world) {
        this.world = world;
        setUpEarth();
    }

    private void setUpEarth() {
        lastGroundChunkPositionX = -Constants.GROUND_WIDTH;
        groundArray = new Array<GroundActor>(Constants.GROUND_CHUNK_COUNT);
        for (int i = 0; i < Constants.GROUND_CHUNK_COUNT; i++) {
            lastGroundChunkPositionX += Constants.GROUND_WIDTH;
            groundArray.add(
                    new GroundActor(WorldUtils.createGround(world, lastGroundChunkPositionX)));
        }
    }

    @Override
    public void act(float delta) {
        for (int i = 0; i < Constants.GROUND_CHUNK_COUNT; i++) {
            GroundActor groundActor = groundArray.get(i);
            if (cameraPositionX - groundActor.getWorldPosition().x > Constants.GROUND_CHUNK_DELETION_DISTANCE) {
                CoreLogger.logDebug("Moving ground chunk.");
                lastGroundChunkPositionX += Constants.GROUND_WIDTH;
                groundActor.setWorldPositionX(lastGroundChunkPositionX);
            }
        }
    }

    public void setCameraPositionX(float cameraPositionX){
        this.cameraPositionX = cameraPositionX;
    }

    public Array<GroundActor> getGroundArray() {
        return this.groundArray;
    }
}
