package com.fuzzywave.slippery.actor;


import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.fuzzywave.slippery.box2d.UserData;

public abstract class GameActor extends Actor {

    protected Body body;
    protected UserData userData;

    public GameActor(Body body) {
        this.body = body;
        this.userData = (UserData) body.getUserData();
    }

    public abstract UserData getUserData();

    public Vector2 getWorldPosition(){
        return body.getPosition();
    }

    public void setWorldPositionX(float x){
        body.setTransform(x, body.getPosition().y, body.getAngle());
    }
}
