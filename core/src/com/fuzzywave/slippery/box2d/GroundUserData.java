package com.fuzzywave.slippery.box2d;


public class GroundUserData extends UserData{

    public GroundUserData() {
        super();
        userDataType = UserDataType.GROUND;
    }
}
