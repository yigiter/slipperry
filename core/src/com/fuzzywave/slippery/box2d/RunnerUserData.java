package com.fuzzywave.slippery.box2d;


import com.badlogic.gdx.math.Vector2;
import com.fuzzywave.slippery.util.Constants;

public class RunnerUserData extends UserData{

    private Vector2 jumpingLinearImpulse;

    public RunnerUserData() {
        super();
        userDataType = UserDataType.RUNNER;
        jumpingLinearImpulse = Constants.RUNNER_LINEAR_IMPULSE;
    }

    public Vector2 getJumpingLinearImpulse() {
        return jumpingLinearImpulse;
    }
}
