package com.fuzzywave.slippery;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.fuzzywave.core.IAnalytics;
import com.fuzzywave.slippery.assets.Assets;
import com.fuzzywave.slippery.screen.SplashScreen;

public class SlipperyGame extends Game {

    public static IAnalytics analytics;

    public SlipperyGame(IAnalytics analytics){
        if(analytics == null){
            throw new IllegalArgumentException("Analytics can not be null");
        }
        SlipperyGame.analytics = analytics;
        SlipperyGame.analytics.init();
    }

	@Override
	public void create () {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);

        SplashScreen splashScreen = new SplashScreen(this);
        this.setScreen(splashScreen);
	}

	@Override
	public void render () {
        Gdx.graphics.getGL20().glClearColor(0, 0, 0, 1);
        Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        super.render();
	}

    @Override
    public void dispose() {
        super.dispose();
        Assets.getInstance().unloadAssets();
        Assets.dispose();
    }
}
